use libiw_sys::*;
use nix::ioctl_read_bad;
ioctl_read_bad!(iw_get_priv_info, SIOCGIWPRIV, iwreq);
fn main() {
    let mut iw: iwreq = unsafe { std::mem::zeroed() };
    let mut a: [u8; 568] = [0; 568];
    let mut res = 0;
    println!("{}", std::mem::size_of::<iw_range>());
    let s = unsafe {
        iw.ifr_ifrn.ifrn_name[0] = 'w' as libc::c_char;
        iw.ifr_ifrn.ifrn_name[1] = 'l' as libc::c_char;
        iw.ifr_ifrn.ifrn_name[2] = 'a' as libc::c_char;
        iw.ifr_ifrn.ifrn_name[3] = 'n' as libc::c_char;
        iw.ifr_ifrn.ifrn_name[4] = '0' as libc::c_char;
        iw.u.data.pointer = a.as_mut_ptr() as *mut libc::c_void;
        iw.u.data.length = a.len() as u16;
        iw.u.data.flags = 0;
        let s = socket(libc::AF_INET, libc::SOCK_DGRAM, 0);
        res = libc::ioctl(s, SIOCGIWRANGE as u64, &iw);

        s
    };
    if res == 0 {
        for b in &a[..] {
            print!("{:02X}:", b);
        }
        println!("\n{} {}", res, a.len());
    } else {
        eprintln!("Bailed {}", std::io::Error::last_os_error());
    }
}
