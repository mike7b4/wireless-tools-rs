extern crate bindgen;
use std::path::PathBuf;

fn main() {
    if !cfg!(target_os = "linux") {
        panic!("Only Linux supported for now, feel free to add more platforms in the UNIX family.");
    }

    println!("cargo:rerun-if-changed=wrapper.h");
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .generate()
        .expect("Could not generate wireless bindings from kernel");

    bindings
        .write_to_file(
            PathBuf::from(std::env::var("OUT_DIR").expect("Missing OUT_DIR")).join("bindings.rs"),
        )
        .expect("Could not write bindings!");
}
